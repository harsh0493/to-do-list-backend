package config

import (
	"fmt"
	"gorm.io/gorm"
)

var DB *gorm.DB

// DBConfig represents db configuration
type DBConfig struct {
	Host     string
	Port     int
	User     string
	DBName   string
	Password string
}

func BuildDBConfig() *DBConfig {
	dbConfig := DBConfig{
		Host:     "rds-mysql-todolist.cti4phzkc6ut.ap-south-1.rds.amazonaws.com",
		Port:     3306,
		User:     "admin",
		DBName:   "todos",
		Password: "mysql_aws_2021",
	}
	return &dbConfig
}

func BuildTestDBConfig() *DBConfig {
	dbConfig := DBConfig{
		Host:     "rds-mysql-todotest.cti4phzkc6ut.ap-south-1.rds.amazonaws.com",
		Port:     3306,
		User:     "admin",
		DBName:   "test_todos",
		Password: "mysql_aws_2021",
	}
	return &dbConfig
}

func DbURL(dbConfig *DBConfig) string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.DBName,
	)
}