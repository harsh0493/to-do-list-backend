package helpers

import (
	"fmt"

	"toDoList/config"
	"toDoList/models"
	"gorm.io/gorm"
	"gorm.io/driver/mysql"
)

var err error

func InitDbConn() error {
	config.DB, err = gorm.Open(mysql.Open(config.DbURL(config.BuildDBConfig())), &gorm.Config{})

	if err != nil {
		fmt.Println("status: ", err)
		return err
	}

	config.DB.AutoMigrate(&models.Todo{})

	return nil
}