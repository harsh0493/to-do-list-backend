package main

import (
	"fmt"
	"toDoList/config"
	"toDoList/models"
	"toDoList/routes"
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gorm.io/gorm"
	"gorm.io/driver/mysql"
	"github.com/stretchr/testify/assert"
)

func performRequest(r http.Handler, method, path string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestToDoCRUD(t *testing.T) {
	config.DB, err = gorm.Open(mysql.Open(config.DbURL(config.BuildTestDBConfig())), &gorm.Config{})

	if err != nil {
		fmt.Println("status: ", err)
	}

	// Drop table if exists (will ignore or delete foreign key constraints when dropping)
	config.DB.Migrator().DropTable(&models.Todo{})
	config.DB.Migrator().DropTable("todo")

	config.DB.AutoMigrate(&models.Todo{})
	
	router := routes.SetupRouter()

	t.Run("Create Empty DB", func(t *testing.T) {
		w := performRequest(router, "GET", "/api/todo")

		assert.Equal(t, http.StatusOK, w.Code)
	})

	t.Run("Populate DB with Test ToDos", func(t *testing.T) {
		todos := []string{
			"Test Title 1",
			"Test Title 2",
			"Test Title 3",
			"Test Title 4",
			"Test Title 5",
			"Test Title 6",
			"Test Title 7",
		}

		for _, todo := range todos {

			payload, _ := json.Marshal(models.Todo{
				Title:  todo,
			})

			req, err := http.NewRequest("POST", "/api/todo", bytes.NewReader(payload))
			req.Header.Set("Content-Type", "application/json")

			w := httptest.NewRecorder()
			router.ServeHTTP(w, req)

			assert.Equal(t, nil, err)
			assert.Equal(t, http.StatusOK, w.Code)
		}
	})
}
