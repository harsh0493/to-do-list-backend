# TO DO LIST BACKEND

It is just a simple REST API written in Go programming language using:
1. **Gin Framework**
2. **Gorm** 

## Architectural Decisions

**Structure of the code**
- MVC pattern has been used in this App to avoid circular dependencies of modules and keep the code clean. Note: Circular dependencies between modules will prevent your app from compiling successfully.

**Framework used**
- GIN framework has been used to create REST APIs in this App. GIN framework is better in terms of performance as compared with other GO frameworks.

**Database Used**
- MySQL RDBMS has been used in this App to store a persistent state of the ToDo list.

## DB Setup
1. Go to config/config.go
2. Update DBName, User, Password and Host, Port according to your database configuration

## Installation & Run

**Install Dependencies**
```
go mod download
```

**Run Tests**
```
go test
```

**Run Application**
```
go run main.go
```

## Build

```
go build
```

To generate executable file with a specific name use -o flag. Ex:
```
go build -o <output file name>
```

## API list

* `GET` : Get all todos
* `POST` : Create a todo

## Post Params
```
{
	"Title": "Sample ToDo task"
}
```

## Run the App in Docker container

```
docker build -t to-do-list-backend . && docker run -p 80:80 to-do-list-backend
```