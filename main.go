package main

import (
	"toDoList/helpers"
	"toDoList/routes"
)

var err error

func main() {

	helpers.InitDbConn()

	r := routes.SetupRouter()
	// running
	r.Run()
}
